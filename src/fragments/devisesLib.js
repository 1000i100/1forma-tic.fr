
const temps_min_par_mois_abonnement = 0.25;
const temps_max_par_mois_abonnement = 10;
const taux_horaire_abonnement = 60;
const taux_horaire_prépayé = 75;
const taux_horaire_plein_tarif = 120;

function MN(euro){ return euro / 300 ; }
function DU(euro){ return MsurN_DU * MN(euro) ; }
function G1(euro){ return G1parDU * DU(euro) ; }
function Miel(euro){ return euro ; }

const fm = (t,decimal=2)=>Math.floor(t)!==t?`${decimal<=2?t.toFixed(decimal):Math.round(t*Math.pow(10,decimal))/Math.pow(10,decimal)}`.replace('.',','):`${t}`;
const ft = (t)=>t==.25?'1/4h':t==.5?'1/2h':t==.75?'3/4h':`${Math.floor(t)}h${(t-Math.floor(t))*60||''}`;
function plafond(temps_mensuel){return Math.round(temps_mensuel*(1+35*Math.pow(10*temps_mensuel,-0.6)));}


function htmlPrix(euro){
    return `<span class="prix">
 <span class="euro"><span class="montant">${fm(euro)}</span><span class="monnaie">€</span></span>
 <span class="DUG1"><span class="montant">${fm(DU(euro))}</span><span class="monnaie">DU<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span></span>
 <span class="G1"><span class="montant">${fm(G1(euro))}</span><span class="monnaie"><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></span></span>
 <span class="MNG1"><span class="montant">${fm(MN(euro),4)}</span><span class="monnaie">M/N<sub><img src="img/G1June.svg" alt="Ǧ1" class="textSized"/></sub></span></span>
 <span class="miel"><span class="montant">${fm(Miel(euro))}</span><span class="monnaie"><img src="img/Miel.svg" alt="Miel" class="textSized"/></span></span>
 </span>`
}
