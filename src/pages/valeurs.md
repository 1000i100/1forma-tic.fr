## Liberté

### Une technologie qui vous libère, selon moi c'est une technologie qui :

- Vous fait gagner du temps.
- Vous laisse indépendant de tout prestataire, fournisseur ou logiciel en vous permettant d'importer/exporter
  vos données sans perte.
- Peut etre maintenue par différents prestataires, grace à une licence libre, gage de pérénité.

### Un prestataire qui vous libère, selon moi c'est quelqu'un qui :

- Vous recommande des logiciels qui vous libèrent (quand ils existent et répondent à vos besoins).
- Vous fournis les code d'accès nécessaire pour faire appel à un confrère sans difficulté si vous souhaitez changer
  de crémière.
- Vous explique les implications des solutions qu'il préconise pour vos besoins, en restant prêt à opter pour
  autre chose si vous lui demandez.

## Simplicité

### Pour vous

- Quand il y a mille façon d'arriver à un résultat, je vous propose ce qui me parait le plus simple en priorité.
- Des solutions au plus prêt de vos besoins. Je n'aime pas pousser à la consomation, et pour m'assurer que
  cela reste ainsi, je ne prend pas d'intéressement à la vente.
- Si je peux vous dépanner dans la minute, à distance, pourquoi s'en priver ? Vous serez plus vite satisfait,
  et je n'aurais pas besoin de plannifier un déplacement chez vous.
- Contactez-moi quand vous voulez (7j/7 24h/24 par email ou sms), je vous répondrai une fois disponnible.

### Pour moi

- Je ne vous vend que des heures de mon temps (et de mon savoir faire).
- Pas de plan sur la comète, je peux vous promettre de vous consacrer du temps, pas de décrocher la lune.
- Je ne m'engage pas sur les délais, mais je ne vous facture que ce qui est fait (et je vous livre au fur et à
  mesure, tout ce qui est payé).

## Confidentialité

- Lorsqu'ils existes, je vous recommande des services respectant votre vie privée.
- Lorsque je peux vous aider à adopter de meilleurs pratiques de sécurité, je le fais (pour éviter piratages,
  pertes de donnéees ou autres désagréments).
- Lorsque vous me confiez des données, elle sont stocké dans ma propre infrastructure, dont je suis le seul
  administrateur, et ne sont partagé à aucun tiers ni utilisé pour autre chose que ce pour quoi vous me les
  avez confiées.

<!-- TODO: charte -->

