## Logs serveurs
**Log :** Compte-rendu automatique d’une activité informatique ; trace, journal,
historique. *Source : [Wiktionnaire](https://fr.wiktionary.org/wiki/log#Nom_commun_2).*

En informatique, le concept d'historique des événements ou de logging désigne l'enregistrement
séquentiel dans un fichier de tous les événements affectant un processus particulier (application,
activité d'un réseau informatique…).
Le journal (en anglais log file ou plus simplement log), désigne alors le fichier contenant ces
enregistrements.
Généralement datés et classés par ordre chronologique, ces derniers permettent d'analyser pas à pas
l'activité interne du processus et ses interactions avec son environnement.
*Source : [Wikipédia](https://fr.wikipedia.org/wiki/Historique_(informatique)).*

### Usage :

- La journalisation (écriture et conservation de logs) est une technique importante de sécurité
  informatique. Elle permet de retracer ce qu'il s'est passé lors d'un piratage par exemple,
  que ce soit pour réparer les dégâts ou pour identifier l'attaquant.
- La journalisation permet d'effectuer des analyses diverses, généralement statistiques et de faire
  des hypothèses sur les dysfonctionnements techniques.
- Les enregistrements d'évènements peuvent également avoir une importance légale : hébergeurs et
  fournisseurs d'accès à Internet sont tenus de fournir un historique des connexions à leur services.

*Sources : [Wikipédia](https://fr.wikipedia.org/wiki/Historique_(informatique)#Applications)
et [Obligations des hébergeurs](http://hebergeur.blog.lemonde.fr/2016/01/07/duree-legale-de-conservation-des-logs-1-an/).*
