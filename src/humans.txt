/* TEAM */
Author, Developpement, Design, Content : [1000i100] Millicent Billette de Villemeur
Location: Bordeaux, France.

/* THANKS */
Name: Marie Nau et [Tornade] Caroline Gaube Barbary, pour leurs précieuses relectures

/* SITE */
Last update: {build:now}
Standards: HTML5, CSS3+Stylus, ES6(JavaScript)
Components: VanillaJS
Software: Gitlab, Gitlab-CI, WebStorm, Atom, Git, Inkscape, Gimp, Darktable
