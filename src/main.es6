import '../node_modules/jparticles/production/jparticles';
import '../node_modules/jparticles/production/particle';

let nextScroll;

function scrollTo(dest, duration = 500) {
	const frequence = 10;
	if(window.getComputedStyle(dest).position === 'fixed') return;
	const to = Math.min(pageOffsetTop(dest), document.body.clientHeight - window.innerHeight);
	let timeLeft = duration;
	nextScroll = () => {
		if ((document.body.scrollTop || document.documentElement.scrollTop || 0) === to) {
			nextScroll = () => 0;
			return;
		}
		const currPos = window.pageYOffset + (to - window.pageYOffset) / (timeLeft / frequence);
		timeLeft -= frequence;
		document.body.scrollTop = document.documentElement.scrollTop = currPos;
		setTimeout(nextScroll, frequence);
	};
	nextScroll();
}
function pageOffsetTop(node){
	return node.offsetTop + (node.offsetParent? pageOffsetTop(node.offsetParent):0);
}

function highlight(target) {
	target.classList.add("highlight");
	setTimeout(() => target.classList.remove("highlight"), 500);
}
function bgCloseBuilder(side){
	const bgClose = document.createElement("a");
	bgClose.classList.add(`bg${side}`);
	bgClose.classList.add("bgPopupClose");
	bgClose.href = "#";
	bgClose.title = "Fermer";
	return bgClose;
}
document.querySelectorAll('.subPage').forEach( popup => {
	const fermer = document.createElement("a");
	fermer.classList.add("close");
	fermer.href = "#";
	fermer.title = "Fermer";
	fermer.innerHTML = "X";
	popup.appendChild(fermer);
	popup.appendChild(bgCloseBuilder('Top'));
	popup.appendChild(bgCloseBuilder('Left'));
	popup.appendChild(bgCloseBuilder('Right'));
	popup.appendChild(bgCloseBuilder('Bottom'));
} );
const baseTitle = ""+document.title;
document.querySelectorAll('a[href^="#"], a[href^="#"] *').forEach(
	link => link.addEventListener('click',
		e => {
			if(!e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey){
				e.preventDefault();
				const link = ancestorMatching((m)=>m.href,e.target);
				if(link.href.split('#')[1]){
					const target = document.getElementById(link.href.split('#')[1]);
					const subTitle = target.querySelector('h1,h2,h3,h4,h5,h6');
					document.title = `${subTitle?subTitle.innerText:'Accueil'} ~ ${baseTitle}`;
				} else {
					document.title = baseTitle;
				}
				history.pushState({},document.title,link.href);
				sessionNav();
				if(link.href.split('#')[1]){
					const target = document.getElementById(link.href.split('#')[1]);
					scrollTo(target);
					highlight(target);
				}
            }
		}
	)
);
function ancestorMatching(matcher, domNode) {
	return matcher(domNode) ? domNode : ancestorMatching(matcher, domNode.parentNode);
}
sessionNav();
function sessionNav(){
    document.querySelectorAll('.subPage').forEach( section => section.classList.add("inactive") );
	document.querySelectorAll(`*`).forEach( link => link.classList.remove("active") );
	document.body.classList.remove("starter");
	document.body.classList.remove("noScroll");
	scrollBarShift(0);
	const widthWithScrollBar = document.body.offsetWidth;
	if(location.hash){
		const activeTarget = document.querySelector(`${location.hash}`);
		activeTarget.classList.add("active");
		activeTarget.classList.remove("inactive");
		document.querySelectorAll(`a[href="${location.hash}"]`).forEach(ref=>ref.classList.add("active") );
		if(activeTarget.classList.contains("subPage")){
			document.body.classList.add("noScroll");
			const widthNoScrollBar = document.body.offsetWidth;
			scrollBarShift(widthNoScrollBar-widthWithScrollBar);
			if(window.pageYOffset < window.innerHeight) document.body.classList.add("starter");
		}
	}
}
function scrollBarShift(size){
	document.querySelectorAll("nav, body>section").forEach(e => e.style.paddingRight = size+"px");
	document.querySelectorAll("body>aside").forEach(e => e.style.right = size+"px");
}
document.getElementById('osm').src = `https://www.openstreetmap.org/export/embed.html?bbox=-0.55545%2C44.81039%2C-0.551907%2C44.81212&layer=mapnik&marker=44.81126%2C-0.55367`;

const moneySwitchInputs = document.querySelectorAll('#moneySwitch input');
const moneyList = [];
moneySwitchInputs.forEach(
    node => {
    	moneyList.push(node.value);
    	node.addEventListener('change',switchMoney)
    }
);
function switchMoney() {
    document.querySelectorAll('.'+moneyList.join(', .')).forEach( node => node.classList.add("inactive") );
    const activeMoney = '.'+getRadioVal(document.getElementById('moneySwitch'), "monnaie"); // was document.getElementById('moneySwitch').monnaie.value without IE :(
    console.log(activeMoney);
    document.querySelectorAll(activeMoney).forEach( node => node.classList.remove("inactive") );
}
switchMoney();


new JParticles.particle('#particles',{
	opacity: .7,
	//resize: false,
	color: ['rgba(255,255,255,.2)', 'rgba(255,255,255,.4)', 'rgba(255,255,255,.5)', 'rgba(255,255,255,.6)','rgba(255,255,255,.8)','#fff', '#7af', '#ad0','#d84'],
	//num: .15,
	//range: 0.99,
	range: 200,
	//proximity: 0.04,
	proximity: 75,
	maxSpeed: 0.1,
	minSpeed: 0.01,
	eventElem: document.getElementById("splash")
/*	,parallaxLayer: [1, 2, 3, 0],
	parallaxStrength: 30,
	parallax: true
*/
});


function getRadioVal(form, name) {
	if(form[name].value) return form[name].value;
	/**
	 * THE IE/EDGE sucks part :
	 */
	let val;
	// get list of radio buttons with specified name
	const radios = form.elements[name];

	// loop through list of radio buttons
	for (let i=0, len=radios.length; i<len; i++) {
		if ( radios[i].checked ) { // radio checked?
			val = radios[i].value; // if so, hold its value in val
			break; // and break out of for loop
		}
	}
	return val; // return value of checked radio or undefined if none checked
}
