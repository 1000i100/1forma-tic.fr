==================================================================
https://keybase.io/1000i100
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://1forma-tic.fr
  * I am 1000i100 (https://keybase.io/1000i100) on keybase.
  * I have a public key ASB6I_LOkXz_AhSMEO4S4S-yv_jDMj3sRZSfdKHWc9wmwAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01207a23f2ce917cff02148c10ee12e12fb2bff8c3323dec45949f74a1d673dc26c00a",
      "host": "keybase.io",
      "kid": "01207a23f2ce917cff02148c10ee12e12fb2bff8c3323dec45949f74a1d673dc26c00a",
      "uid": "f1bf638d1eba1de8f396988182ed9e19",
      "username": "1000i100"
    },
    "merkle_root": {
      "ctime": 1522895487,
      "hash": "ab8369bb4a5ff441bae8841c55b9a73f7ee11b8c1c2c5d610bfb7f6cea66486f35c0cfc0884f33d748e75536f93d037e63087fa7611a3ad7132ddcf1b6455f7f",
      "hash_meta": "697c34faaddcff7f5520bf68f3b7bdef3e8b9efa45b045f3685a155f9d14edd8",
      "seqno": 2338492
    },
    "service": {
      "entropy": "A5AfzXMOVNkjj8LPU6JPL9SJ",
      "hostname": "1forma-tic.fr",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.47"
  },
  "ctime": 1522895526,
  "expire_in": 504576000,
  "prev": "a5c84b3d2ce936fa0ac8e9305197ef04fbe21618ec60240ff2232adf7a9fbcbc",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgeiPyzpF8/wIUjBDuEuEvsr/4wzI97EWUn3Sh1nPcJsAKp3BheWxvYWTESpcCB8QgpchLPSzpNvoKyOkwUZfvBPviFhjsYCQP8iMq33qfvLzEIOZUKGcQzrcBjwIzlbvhmTY02FWgubJQEdUVE5qOGa23AgHCo3NpZ8RA9MELZQmia/FJEfEoHlPTRJ+KxNEbEMjk4fvpml3lkJateAVsabPTx0bNN85r71Q41MSl+p2IsvYdMpYYw+GIBKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIFR0RcHm+r/bNJR5jfhDn61/0mTQiIKmRWdySbfjT1+Jo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/1000i100

==================================================================
